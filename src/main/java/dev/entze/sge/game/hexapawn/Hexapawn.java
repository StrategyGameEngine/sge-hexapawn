package dev.entze.sge.game.hexapawn;

import dev.entze.sge.game.ActionRecord;
import dev.entze.sge.game.Game;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Hexapawn implements Game<String, Integer[]> {

  public static final int SOUTH = 0;
  public static final int NORTH = 1;
  public static final int EMPTY = 2;
  public static final Integer[] STARTING_BOARD = new Integer[] {
      SOUTH, SOUTH, SOUTH,
      EMPTY, EMPTY, EMPTY,
      NORTH, NORTH, NORTH};

  private static final String algebraicNotationMoveRegex = "^[abc][123]$";
  private static final String algebraicNotationCaptureRegex = "^(axb[123])|(bx[ac][123])|(cxb[123])$";
  private static final Pattern algebraicNotationPattern = Pattern
      .compile(algebraicNotationMoveRegex + "|" + algebraicNotationCaptureRegex);
  private final boolean canonical;
  private final List<ActionRecord<String>> actionRecords;
  private int currentPlayer;
  private Integer[] board;

  public Hexapawn() {
    this(SOUTH, true, Collections.emptyList(), STARTING_BOARD);
  }

  public Hexapawn(String startingPosition) {
    this();
  }

  public Hexapawn(int numberOfPlayers) {
    this();
    if (numberOfPlayers != 2) {
      throw new IllegalArgumentException("2 Player Game only");
    }
  }

  public Hexapawn(String startingPosition, int numberOfPlayers) {
    this(numberOfPlayers);
  }

  public Hexapawn(int numberOfPlayers, int currentPlayer, boolean canonical,
      List<ActionRecord<String>> actionRecords, Integer[] board) {
    this(currentPlayer, canonical, actionRecords, board);
    if (numberOfPlayers != 2) {
      throw new IllegalArgumentException("2 Player Game only");
    }
  }

  public Hexapawn(int currentPlayer, boolean canonical,
      List<ActionRecord<String>> actionRecords,
      Integer[] board) {
    this.currentPlayer = currentPlayer;
    this.canonical = canonical;
    this.actionRecords = new ArrayList<>(actionRecords);
    this.board = board.clone();
    if (board.length != 9) {
      throw new IllegalArgumentException("Board malformed");
    }
  }


  @Override
  public boolean isGameOver() {
    boolean southHasMovesLeft = false;
    boolean northHasMovesLeft = false;
    for (int i = 0; i < board.length; i++) {
      if (board[i] == SOUTH) {
        if (i > 5) {
          return true;
        } else if (board[i + 3] == EMPTY) {
          southHasMovesLeft = true;
        } else if (i % 3 < 2 && board[i + 4] == NORTH) {
          southHasMovesLeft = true;
        } else if (i % 3 > 0 && board[i + 2] == NORTH) {
          southHasMovesLeft = true;
        }
      } else if (board[i] == NORTH) {
        if (i < 3) {
          return true;
        } else if (board[i - 3] == EMPTY) {
          northHasMovesLeft = true;
        } else if (i % 3 > 0 && board[i - 4] == SOUTH) {
          northHasMovesLeft = true;
        } else if (i % 3 < 2 && board[i - 2] == SOUTH) {
          northHasMovesLeft = true;
        }
      }
    }
    return (currentPlayer == NORTH && !northHasMovesLeft) || (currentPlayer == SOUTH
        && !southHasMovesLeft);
  }

  @Override
  public int getMinimumNumberOfPlayers() {
    return 2;
  }

  @Override
  public int getMaximumNumberOfPlayers() {
    return 2;
  }

  @Override
  public int getNumberOfPlayers() {
    return 2;
  }

  @Override
  public int getCurrentPlayer() {
    return this.currentPlayer;
  }

  @Override
  public double getPlayerUtilityWeight(int player) {
    if (player == this.currentPlayer) {
      return 1;
    }
    return 0;
  }

  @Override
  public double getUtilityValue(int player) {
    int possibleMoves = 0;
    for (int sq = 0; sq < board.length; sq++) {
      if (sq <= 2 && board[sq] == NORTH) {
        return player; //0 -> SOUTH lost
      }
      if (sq >= 6 && board[sq] == SOUTH) {
        return 1D - player; //0 -> NORTH lost
      }
      if (board[sq] != EMPTY) {
        assert (board[sq] != NORTH) || (0 <= sq - 3 && sq - 3 < board.length);
        assert (board[sq] != SOUTH) || (0 <= sq + 3 && sq + 3 < board.length);
        if ((board[sq] == SOUTH && board[sq + 3] == EMPTY)
            || (board[sq] == NORTH && board[sq - 3] == EMPTY)) {
          possibleMoves++;
        }
      }
    }
    if (possibleMoves <= 0) {
      return (currentPlayer == player ? (0D) : 1D);
    }
    return 1D / 2D;
  }

  @Override
  public double getHeuristicValue(int player) {
    double heuristicValue = 0;
    for (int sq = 0; sq < board.length; sq++) {
      if (board[sq] == player) {
        heuristicValue += (2 * player - (sq / 3)) + 1D;
      }
    }
    return heuristicValue;
  }

  @Override
  public Set<String> getPossibleActions() {
    Set<String> actionSet = new TreeSet<>();
    int cpm = 1 - 2 * currentPlayer; //current player multiplier 0 -> 1; 1 -> (-1)
    for (int sq = 0; sq < 9; sq++) {
      if (sq < 3 && board[sq] == NORTH) {
        return Collections.emptySet();
      } else if (sq > 5 && board[sq] == SOUTH) {
        return Collections.emptySet();
      }
      if (board[sq] == currentPlayer) {
        int destSq = sq + 3 * cpm;
        if ((0 <= destSq && destSq < board.length) && board[destSq] == EMPTY) {
          actionSet.add(squareNumberToAlgebraicNotation(destSq));
        }
        if ((sq % 3 <= 1 && currentPlayer == SOUTH) || (sq % 3 >= 1 && currentPlayer == NORTH)) {
          destSq = sq + 4 * cpm;
          if ((0 <= destSq && destSq < board.length) && board[destSq] != EMPTY
              && board[destSq] != currentPlayer) {
            actionSet
                .add("" + ((char) ('a' + sq % 3)) + 'x' + squareNumberToAlgebraicNotation(destSq));
          }
        }
        if ((sq % 3 >= 1 && currentPlayer == SOUTH) || (sq % 3 <= 1 && currentPlayer == NORTH)) {
          destSq = sq + 2 * cpm;
          if ((0 <= destSq && destSq < board.length) && board[destSq] != EMPTY
              && board[destSq] != currentPlayer) {
            actionSet
                .add("" + ((char) ('a' + sq % 3)) + 'x' + squareNumberToAlgebraicNotation(destSq));
          }
        }
      }
    }
    return actionSet;
  }

  @Override
  public Integer[] getBoard() {
    return board.clone();
  }

  private boolean isAlgebraicNotation(String action) {
    Matcher matcher = algebraicNotationPattern.matcher(action);
    return matcher.matches();
  }

  private int getDestSqr(String action) {
    char column = action.charAt(action.length() - 2);
    char row = action.charAt(action.length() - 1);

    return (column - 'a') + (row - '1') * 3;
  }

  private int getSrcSqr(String action, int destSqr, int currentPlayer) {
    int cpm = 1 - 2 * currentPlayer; //current player multiplier 0 -> 1; 1 -> (-1)
    int srcSqr = destSqr - 3 * cpm;
    if (action.length() == 2) {
      return srcSqr;
    }

    int moveDirection = action.charAt(0) - action
        .charAt(action.length() - 2); //-1 -> from left to right, 1 -> from right to left

    srcSqr += moveDirection;

    return srcSqr;

  }

  @Override
  public boolean isValidAction(String action) {
    if (!isAlgebraicNotation(action) || isGameOver()) {
      return false;
    }

    int destSqr = getDestSqr(action);
    int srcSqr = getSrcSqr(action, destSqr, currentPlayer);

    if (board[srcSqr] != currentPlayer) {
      return false;
    }

    if (action.length() == 2) {
      return board[destSqr] == EMPTY;
    }

    return board[destSqr] == 1 - currentPlayer;

  }

  @Override
  public Game<String, Integer[]> doAction(String action) {
    if (action == null || !isAlgebraicNotation(action)) {
      throw new IllegalArgumentException("Invalid algebraic notation.");
    }

    if (isGameOver()) {
      throw new IllegalArgumentException("Game is over.");
    }

    int destSq = getDestSqr(action);
    int srcSq = getSrcSqr(action, destSq, currentPlayer);

    if (board[srcSq] != currentPlayer) {
      throw new IllegalArgumentException("No own pawn on source square.");
    } else if (action.length() == 2) {
      if (board[destSq] != EMPTY) {
        throw new IllegalArgumentException("Cannot move vertically onto a non-empty square.");
      }
    } else if (board[destSq] != 1 - currentPlayer) {
      throw new IllegalArgumentException(
          "Cannot capture if no enemy present on destination square.");
    }

    Hexapawn newGame = new Hexapawn(1 - currentPlayer, this.canonical, this.actionRecords,
        board);
    newGame.board[destSq] = newGame.board[srcSq];
    newGame.board[srcSq] = EMPTY;
    newGame.actionRecords.add(new ActionRecord<>(currentPlayer, action));

    return newGame;
  }

  /**
   * Hexapawn is a perfect- and complete-information game, therefore this method only returns null.
   *
   * @return null
   */
  @Override
  public String determineNextAction() {
    return null;
  }

  @Override
  public List<ActionRecord<String>> getActionRecords() {
    return Collections.unmodifiableList(this.actionRecords);
  }

  @Override
  public boolean isCanonical() {
    return this.canonical;
  }

  @Override
  public Game<String, Integer[]> getGame(int player) {
    return new Hexapawn(currentPlayer, false, this.actionRecords, board);
  }

  @Override
  public String toTextRepresentation() {
    StringBuilder textRepresentation = new StringBuilder();
    textRepresentation.append('\n');
    for (int y = 2; y >= 0; y--) {
      textRepresentation.append("  +---+---+---+\n").append(y + 1).append(" |");
      for (int x = 0; x < 3; x++) {
        textRepresentation.append(' ').append(board[y * 3 + x] == Hexapawn.EMPTY ? ' '
            : (board[y * 3 + x] == Hexapawn.SOUTH ? 'S' : 'N')).append(" |");
      }
      textRepresentation.append("\n");
    }
    textRepresentation.append("  +---+---+---+\n    a   b   c\n");
    return textRepresentation.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Hexapawn hexapawn = (Hexapawn) o;
    return currentPlayer == hexapawn.currentPlayer &&
        canonical == hexapawn.canonical &&
        actionRecords.equals(hexapawn.actionRecords) &&
        Arrays.equals(board, hexapawn.board);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(currentPlayer, canonical, actionRecords);
    result = 31 * result + Arrays.hashCode(board);
    return result;
  }

  @Override
  public String toString() {
    return Arrays.toString(board);
  }


  private String squareNumberToAlgebraicNotation(int sqNumber) {
    return "" + ((char) ('a' + sqNumber % 3)) + (sqNumber / 3 + 1);
  }

}
