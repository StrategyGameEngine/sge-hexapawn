package dev.entze.sge.game.hexapawn;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import dev.entze.sge.game.Game;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import org.junit.Before;
import org.junit.Test;

public class HexapawnTest {

  private Random random = new Random();
  private Game<String, Integer[]> hexapawn;

  @Before
  public void setUp() throws Exception {
    hexapawn = new Hexapawn();
  }

  @Test
  public void test_AfterInit() {
    assertEquals("First Player should be to move", 0, hexapawn.getCurrentPlayer());
    assertTrue("Should be canonical", hexapawn.isCanonical());
    assertFalse("Should not be over yet", hexapawn.isGameOver());
  }

  @Test
  public void test_visualRepresentation() {
    assertEquals(
        "\n"
            + "  +---+---+---+\n"
            + "3 | N | N | N |\n"
            + "  +---+---+---+\n"
            + "2 |   |   |   |\n"
            + "  +---+---+---+\n"
            + "1 | S | S | S |\n"
            + "  +---+---+---+\n"
            + "    a   b   c\n",
        hexapawn.toTextRepresentation());
  }

  @Test
  public void test_doAction_1() {
    hexapawn = hexapawn.doAction("b2");
    assertArrayEquals(new Integer[] {0, 2, 0, 2, 0, 2, 1, 1, 1}, hexapawn.getBoard());
  }

  @Test
  public void test_doAction_2() {
    hexapawn = new Hexapawn(1, true, Collections.emptyList(),
        new Integer[] {0, 2, 0, 2, 0, 2, 1, 1, 1});
    hexapawn = hexapawn.doAction("axb2");
    assertArrayEquals(new Integer[] {0, 2, 0, 2, 1, 2, 2, 1, 1}, hexapawn.getBoard());
  }

  @Test
  public void test_doAction_3() {
    hexapawn = hexapawn.doAction("b2");
    hexapawn = hexapawn.doAction("axb2");
    hexapawn = hexapawn.doAction("axb2");
    hexapawn = hexapawn.doAction("cxb2");
    hexapawn = hexapawn.doAction("cxb2");
    assertArrayEquals(new Integer[] {2, 2, 2, 2, 0, 2, 2, 1, 2}, hexapawn.getBoard());
  }

  @Test
  public void test_getUtilityValue_1() {
    hexapawn = new Hexapawn(0, true, Collections.emptyList(),
        new Integer[] {2, 2, 2, 2, 0, 2, 2, 1, 2});
    assertEquals(0, (int) hexapawn.getUtilityValue(0));
    assertEquals(1, (int) hexapawn.getUtilityValue(1));
  }

  @Test
  public void test_getUtilityValue_2() {
    hexapawn = new Hexapawn(1, true, Collections.emptyList(),
        new Integer[] {2, 2, 2, 2, 0, 2, 2, 1, 2});
    assertEquals(1, (int) hexapawn.getUtilityValue(0));
    assertEquals(0, (int) hexapawn.getUtilityValue(1));
  }

  @Test
  public void test_getUtilityValue_3() {
    hexapawn = new Hexapawn(0, true, Collections.emptyList(),
        new Integer[] {2, 2, 2, 2, 0, 2, 2, 1, 2});
    assertEquals(0, (int) hexapawn.getUtilityValue());
  }

  @Test
  public void test_getUtilityValue_4() {
    hexapawn = new Hexapawn(1, true, Collections.emptyList(),
        new Integer[] {2, 2, 2, 2, 0, 2, 2, 1, 2});
    assertEquals(0, (int) hexapawn.getUtilityValue());
  }


  @Test
  public void test_getPossibleActions_1() {
    List<String> possibleActions = new ArrayList<>(hexapawn.getPossibleActions());
    String[] expect = {"a2", "b2", "c2"};
    String[] actual = new String[expect.length];
    for (int i = 0; i < possibleActions.size(); i++) {
      actual[i] = possibleActions.get(i);
    }
    assertArrayEquals(expect, actual);
  }


  @Test
  public void test_getPossibleActions_2() {
    hexapawn = new Hexapawn(1, true, Collections.emptyList(),
        new Integer[] {0, 2, 0, 2, 0, 2, 1, 1, 1});
    List<String> possibleActions = new ArrayList<>(hexapawn.getPossibleActions());
    String[] expect = {"a2", "axb2", "cxb2", "c2"};
    String[] actual = new String[possibleActions.size()];
    for (int i = 0; i < possibleActions.size(); i++) {
      actual[i] = possibleActions.get(i);
    }
    Arrays.sort(expect);
    Arrays.sort(actual);
    assertArrayEquals(expect, actual);
  }

  @Test
  public void test_getPossibleActions_3() {
    hexapawn = new Hexapawn(0, true, Collections.emptyList(),
        new Integer[] {0, 2, 0, 2, 1, 2, 2, 1, 1});
    List<String> possibleActions = new ArrayList<>(hexapawn.getPossibleActions());
    String[] expect = {"a2", "axb2", "cxb2", "c2"};
    String[] actual = new String[possibleActions.size()];
    for (int i = 0; i < possibleActions.size(); i++) {
      actual[i] = possibleActions.get(i);
    }
    Arrays.sort(expect);
    Arrays.sort(actual);
    assertArrayEquals(expect, actual);
  }

  @Test
  public void test_getPossibleActions_4() {
    hexapawn = new Hexapawn();
    Set<String> expectedPossibleActions = new TreeSet<>(Arrays.asList("a2", "b2", "c2"));
    assertEquals(expectedPossibleActions, hexapawn.getPossibleActions());
    hexapawn = hexapawn.doAction("a2");
    expectedPossibleActions = new TreeSet<>(Arrays.asList("bxa2", "b2", "c2"));
    assertEquals(expectedPossibleActions, hexapawn.getPossibleActions());
    hexapawn = hexapawn.doAction("bxa2");
    expectedPossibleActions = new TreeSet<>(Arrays.asList("bxa2", "b2", "c2"));
    assertEquals(expectedPossibleActions, hexapawn.getPossibleActions());
    hexapawn = hexapawn.doAction("b2");
    expectedPossibleActions = new TreeSet<>(Arrays.asList("axb2", "a1", "c2", "cxb2"));
    assertEquals(expectedPossibleActions, hexapawn.getPossibleActions());
  }

  @Test
  public void test_isGameOver_1() {
    hexapawn = new Hexapawn();
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("c2");
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("b2");
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("axb2");
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("axb2");
    assertTrue(hexapawn.isGameOver());
  }

  @Test
  public void test_isGameOver_2() {
    hexapawn = new Hexapawn();
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("c2");
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("bxc2");
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("bxc2");
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("a2");
    assertTrue(hexapawn.isGameOver());

  }

  @Test
  public void test_isGameOver_3() {
    hexapawn = new Hexapawn();
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("a2");
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("bxa2");
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("b2");
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("axb2");
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("c2");
    assertFalse(hexapawn.isGameOver());
    hexapawn = hexapawn.doAction("a1");
    assertTrue(hexapawn.isGameOver());

  }

  @Test
  public void test_getPossibleActions_5() {
    hexapawn = new Hexapawn(Hexapawn.SOUTH, true, Collections.emptyList(),
        new Integer[] {2, 2, 2, 0, 2, 2, 2, 0, 0});
    assertEquals(Collections.emptySet(), hexapawn.getPossibleActions());
  }

  @Test
  public void test_getPossibleActions_6() {
    hexapawn = new Hexapawn(Hexapawn.SOUTH, true, Collections.emptyList(),
        new Integer[] {0, 0, 1, 2, 2, 2, 1, 1, 2});
    assertEquals(Collections.emptySet(), hexapawn.getPossibleActions());
  }

  @Test
  public void hypothesis_getPossibleActions_1() {

    for (int p = 0; p < 2; p++) {
      for (int s0 = 0; s0 < 3; s0++) {
        for (int s1 = 0; s1 < 3; s1++) {
          for (int s2 = 0; s2 < 3; s2++) {
            for (int s3 = 0; s3 < 3; s3++) {
              for (int s4 = 0; s4 < 3; s4++) {
                for (int s5 = 0; s5 < 3; s5++) {
                  for (int s6 = 0; s6 < 3; s6++) {
                    for (int s7 = 0; s7 < 3; s7++) {
                      for (int s8 = 0; s8 < 3; s8++) {
                        hexapawn = new Hexapawn(p, true, Collections.emptyList(),
                            new Integer[] {s0, s1, s2, s3, s4, s5, s6, s7, s8});

                        assertEquals(hexapawn.isGameOver(),
                            hexapawn.getPossibleActions().isEmpty());
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

  }

}
